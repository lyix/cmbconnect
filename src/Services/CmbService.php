<?php
/**
 * Created by PhpStorm.
 * User: ming
 * Date: 18-1-31
 * Time: 上午11:21
 */

namespace Lyix\Cmbconnect\Services;

use Couchbase\Exception;
use Lyix\Cmbconnect\Contracts\Pays;

class CmbService implements Pays
{

    protected $values;

    /**
     * 测试
     *
     * @return string
     */
    public function callMe($controller)
    {
        return 'test' . $controller;
    }

    /**
     * 组合数据xml各post到招商
     * @param array $data
     * @param $url
     * @return mixed
     * @throws Exception
     */
    public function xmlPost(array $data, $url)
    {
        $xml = $this->xmlEncode($data);
        $xml = self::postXmlCurl($xml, $url);
        $res = $this->fromXml($xml);
        return $res;
    }


    /**
     * XML编码
     * @param mixed $data 数据
     * @param string $encoding 数据编码
     * @param string $root 根节点名
     * @return string
     */
    function xmlEncode($data, $encoding = 'GBK', $root = 'CMBSDKPGK')
    {
        $xml = '<?xml version = "1.0" encoding = "' . $encoding . '"?>';
        $xml .= '<' . $root . '>';
        $xml .= $this->dataToXml($data);
        $xml .= '</' . $root . '>';
        return $xml;
    }

    /**
     * 数据XML编码
     * @param mixed $data 数据
     * @return string
     */
    function dataToXml($data)
    {
        $xml = '';
        foreach ($data as $key => $val) {
            is_numeric($key) && $key = "item id=\"$key\"";
            $xml .= "<$key>";
            $xml .= (is_array($val) || is_object($val)) ? $this->dataToXml($val) : $val;
            list($key,) = explode(' ', $key);
            $xml .= "</$key>";
        }
        return $xml;
    }


    /**
     * 以post方式提交xml到对应的接口url
     * @param $xml  需要post的xml数据
     * @param $url
     * @param int $second $second url执行超时时间，默认30s
     * @return mixed
     * @throws Exception
     */

    private static function postXmlCurl($xml, $url, $second = 30)
    {
        $ch = curl_init();
        //设置超时
        curl_setopt($ch, CURLOPT_TIMEOUT, $second);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);//严格校验
        //设置header
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

        //post提交方式
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        //运行curl
        $data = curl_exec($ch);
        //返回结果
        if ($data) {
            curl_close($ch);
            return $data;
        } else {
            $error = curl_errno($ch);
            curl_close($ch);
            throw new Exception("curl出错，错误码:$error");
        }
    }

    /**
     * 将xml转为array
     * @param $xml
     * @return mixed
     */
    public function fromXml($xml)
    {
        if (!$xml) {
            throw new WxPayException("xml数据异常！");
        }
        //转utf8
        $xml = str_replace('GBK', 'UTF-8', $xml);
        //将XML转为array
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $this->values = json_decode(json_encode(simplexml_load_string($xml, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        return $this->values;
    }


}