<?php
/**
 * Created by PhpStorm.
 * User: ming
 * Date: 18-1-31
 * Time: 下午3:10
 */

namespace Lyix\Cmbconnect\Contracts;

interface Pays
{
    public function callMe($controller);

    public function xmlPost(array $data, $url);


}