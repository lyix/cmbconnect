<?php

namespace Lyix\Cmbconnect\Providers;

use Illuminate\Support\ServiceProvider;
use Lyix\Cmbconnect\Services\CmbService;

class PayServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

        $this->app->bind('Pays', function () {
            return new CmbService();
        });
    }
}
